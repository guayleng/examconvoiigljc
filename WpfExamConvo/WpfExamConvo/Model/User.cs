﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfExamConvo
{
    class User
    {
        public String name { get; set; }
        public String correo { get; set; }

        public User(string name, string correo)
        {
            this.name = name;
            this.correo = correo;
        }
    }
}
